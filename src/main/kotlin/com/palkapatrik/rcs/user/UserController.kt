package com.palkapatrik.rcs.user

import com.palkapatrik.rcs.dtos.*
import com.palkapatrik.rcs.shared.CommonHelpers
import com.palkapatrik.rcs.shared.JwtService
import com.palkapatrik.rcs.shared.ResponseMessages
import com.palkapatrik.rcs.shared.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping(Routes.USERS)
class UserController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var jwtService: JwtService

    @GetMapping("/")
    fun getAllUsers() = userService.getAllUsers()

    @GetMapping("/{id}")
    fun getUser(@PathVariable id: String) = userService.getUser(id)

    @PostMapping("/authenticate")
    fun authenticate(@RequestBody body: LoginDTO, response: HttpServletResponse): ResponseEntity<Any> {
        val user = userService.authenticateUser(body)
        return if (user != null) {
            val tokenCookie = jwtService.createToken(user.id)
            response.addCookie(tokenCookie)

            ResponseEntity.ok().body(user)
        } else {
            ResponseEntity.badRequest().body(ErrorMessage(400, "USER_NOT_FOUND", "User or password is incorrect"))
        }
    }

    @GetMapping("/delete/all")
    fun deleteAll(): ResponseEntity<Message> {
        userService.deleteAll()
        return ResponseEntity.ok().body(Message(ResponseMessages.deleted))
    }

    @PostMapping("/signup")
    fun register(@RequestBody body: RegisterDTO): ResponseEntity<Message> {
        val newUser: UserModel? = userService.createNewUser(body)
        return if (newUser != null) {
            ResponseEntity.ok(Message(ResponseMessages.success))
        } else {
            ResponseEntity.badRequest().body(Message(ResponseMessages.error))
        }
    }

    @PostMapping("/update/{id}")
    fun updateData(@PathVariable("id") id: String, @RequestBody body: UserUpdateDTO): ResponseEntity<Any> {
        val userUpdated = userService.updateData(id, body)
        return if (userUpdated) {
            ResponseEntity.ok().body(Message(ResponseMessages.success))
        } else {
            ResponseEntity.badRequest().body(ErrorMessage(400, "USER_UPDATE_FAILED", "User update failed"))
        }
    }
}
