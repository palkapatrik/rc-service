package com.palkapatrik.rcs.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface UserRepository : JpaRepository<UserModel, String> {
    @Query("SELECT u FROM UserModel u WHERE u.email = :email")
    fun existsByEmail(@Param("email") email: String): Boolean
    fun getByEmail(@Param("email") email: String): UserModel?

    @Modifying
    @Transactional
    @Query("UPDATE UserModel u SET u.email = :email, u.fullName = :fullName WHERE u.id = :id")
    fun updateEmailAndNameById(
        @Param("email") email: String,
        @Param("fullName") fullName: String,
        @Param("id") id: String
    )
}
