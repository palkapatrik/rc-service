package com.palkapatrik.rcs.user

import com.palkapatrik.rcs.dtos.LoginDTO
import com.palkapatrik.rcs.dtos.RegisterDTO
import com.palkapatrik.rcs.dtos.UserUpdateDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserService {
    @Autowired
    lateinit var userRepository: UserRepository

    fun getAllUsers(): Iterable<UserModel> = userRepository.findAll()

    fun getUser(id: String) = userRepository.findById(id)

    fun userExists(id: String) = userRepository.existsById(id)

    fun updateData(id: String, user: UserUpdateDTO): Boolean {
        return if (userRepository.existsById(id)) {
            userRepository.updateEmailAndNameById(user.email, user.fullName, id)
            true
        } else {
            false
        }
    }

    fun deleteAll() {
        userRepository.deleteAll()
    }

    fun createNewUser(user: RegisterDTO): UserModel? {
        val uniqueEmail = userRepository.getByEmail(user.email)
        return if (uniqueEmail == null) {
            val newUser = UserModel()
            newUser.email = user.email
            newUser.fullName = user.fullName
            newUser.password = user.password

            userRepository.save(newUser)
        } else {
            print(uniqueEmail)
            null
        }
    }

    fun authenticateUser(loginData: LoginDTO): UserModel? {
        val user = userRepository.getByEmail(loginData.email)
            ?: return null
        return if (user.comparePassword(loginData.password)) {
            user
        } else {
            null
        }
    }
}
