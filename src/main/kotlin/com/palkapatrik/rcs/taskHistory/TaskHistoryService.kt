package com.palkapatrik.rcs.taskHistory

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TaskHistoryService {
    @Autowired
    lateinit var taskHistoryRepository: TaskHistoryRepository

    fun getAllRecords(): Iterable<TaskHistory> = taskHistoryRepository.findAll()

    fun getAllRecordsForUser(userId: String) = taskHistoryRepository.getAllByUserId(userId)

    fun addRecord(userId: String, action: TaskAction) {
        val record = TaskHistory()
        record.userId = userId
        record.action = action
        record.comment = action.comment

        taskHistoryRepository.save(record)
    }

}
