package com.palkapatrik.rcs.taskHistory

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface TaskHistoryRepository : JpaRepository<TaskHistory, String> {
    fun getAllByUserId(@Param("userId") userId: String): Iterable<TaskHistory>
}
