package com.palkapatrik.rcs.taskHistory

enum class TaskAction(val comment: String) {
    ERROR("Something went horribly wrong"),
    CREATE_CONCEPT("{} created new  task"),
    DELETE_CONCEPT("{} deleted task concept"),
    SEND_TASK("{} sent task to next state"),
    ACCEPT_TASK("{} accepted the task"),
    APPROVE("{} approved the task"),
    DENY("{} denied the task"),
    FINISH("{} finished the task")
}
