package com.palkapatrik.rcs.taskHistory

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.*

@Entity
class TaskHistory {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    var id = ""

    @Column
    var userId = ""

    @Column
    var action: TaskAction? = null

    @Column
    var comment = ""

    @Column
    @GeneratedValue
    var date: Date = Date(System.currentTimeMillis())
}
