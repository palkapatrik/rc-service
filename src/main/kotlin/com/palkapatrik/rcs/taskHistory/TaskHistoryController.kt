package com.palkapatrik.rcs.taskHistory

import com.palkapatrik.rcs.shared.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.HISTORY)
class TaskHistoryController {
    @Autowired
    lateinit var taskHistoryService: TaskHistoryService

    @GetMapping("")
    fun getAll(): Iterable<TaskHistory> = taskHistoryService.getAllRecords()

    @GetMapping("/{userId}")
    fun getAllForUser(@PathVariable("userId") userId: String) = taskHistoryService.getAllRecordsForUser(userId)
}
