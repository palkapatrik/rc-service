package com.palkapatrik.rcs.task

import com.palkapatrik.rcs.dtos.ErrorMessage
import com.palkapatrik.rcs.dtos.Message
import com.palkapatrik.rcs.dtos.TaskDTO
import com.palkapatrik.rcs.shared.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Routes.TASKS)
class TaskController {
    @Autowired
    lateinit var taskService: TaskService

    @GetMapping("")
    fun getAllTasks() = taskService.getAllTasks()

    @DeleteMapping("/delete-all")
    fun deleteAllTasks() = taskService.deleteAllTasks()

    @GetMapping("/{id}")
    fun getTask(@PathVariable("id") id: String): ResponseEntity<Any> {
        val task = taskService.getTaskById(id)
        return if (task != null) {
            ResponseEntity.ok().body(task)
        } else {
            ResponseEntity.badRequest().body(ErrorMessage(404, "TASK_NOT_FOUND", "Unable to find task"))
        }
    }

    @PostMapping("/create")
    fun createTask(@RequestBody taskData: TaskDTO): ResponseEntity<Task> {
        val createdTask = taskService.createTask(taskData.assignee, taskData.releaseId)
        return ResponseEntity.ok().body(createdTask)
    }

    @PutMapping("/{id}/assign")
    fun assignTask(@PathVariable("id") id: String, @RequestBody assignee: String): ResponseEntity<Any> {
        val assigned = taskService.assignTask(id, assignee)
        return if (assigned) {
            ResponseEntity.ok().body(Message("Task has been assigned"))
        } else {
            ResponseEntity.badRequest()
                .body(
                    ErrorMessage(400, "UNABLE_TO_ASSIGN_TASK", "Unable to assign task")
                )
        }
    }
}
