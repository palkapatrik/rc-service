package com.palkapatrik.rcs.task

import com.palkapatrik.rcs.taskHistory.TaskAction
import com.palkapatrik.rcs.taskHistory.TaskHistoryService
import com.palkapatrik.rcs.user.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class TaskService {
    @Autowired
    lateinit var taskRepository: TaskRepository

    @Autowired
    lateinit var taskHistoryService: TaskHistoryService

    @Autowired
    lateinit var userService: UserService

    fun getAllTasks(): Iterable<Task> = taskRepository.findAll()

    fun getTaskById(id: String) = taskRepository.findByIdOrNull(id)

    fun deleteAllTasks() = taskRepository.deleteAll()

    fun taskExists(id: String) = taskRepository.existsById(id)

    fun getTasksForUserId(userId: String) = taskRepository.findAllByAssignee(userId)

    fun createTask(authorId: String, releaseId: Int): Task {
        val newTask = Task()
        newTask.assignee = authorId
        newTask.releaseId = releaseId

        taskHistoryService.addRecord(authorId, TaskAction.CREATE_CONCEPT)
        return taskRepository.save(newTask)
    }

    fun assignTask(taskId: String, userId: String): Boolean {
        val canAssign = userService.userExists(userId) && taskExists(taskId)
        return if (canAssign) {
            val taskAssigned = taskRepository.updateAssigneeByTaskId(userId, taskId)
            (taskAssigned == 1)
        } else {
            false
        }
    }

    fun moveTask(taskId: String, action: TaskAction) {

    }
}
