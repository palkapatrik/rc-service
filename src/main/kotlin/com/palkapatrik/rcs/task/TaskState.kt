package com.palkapatrik.rcs.task

enum class TaskState(val value: String) {
    CONCEPT("Concept"),
    APPROVAL("Awaiting assessment"),
    IMPLEMENTATION("Implementation"),
    DONE("Deployed"),
    REJECTED("Rejected implementation"),
    CANCELED("Canceled during implementation")
}
