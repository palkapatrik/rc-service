package com.palkapatrik.rcs.task

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface TaskRepository : JpaRepository<Task, String> {

    fun findAllByAssignee(@Param("assignee") assignee: String): Iterable<Task>

    @Modifying
    @Transactional
    @Query("UPDATE Task t SET t.assignee = :assignee WHERE t.id = :id")
    fun updateAssigneeByTaskId(@Param("assignee") assignee: String, @Param("id") id: String): Int
}
