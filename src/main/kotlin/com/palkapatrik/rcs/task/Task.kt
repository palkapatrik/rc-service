package com.palkapatrik.rcs.task

import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
class Task {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    var id = ""

    @Column
    var state = TaskState.CONCEPT

    @Column
    var assignee = ""

    @Column(nullable = true)
    var releaseId: Int? = null
    // TODO releaseId should never be null
}
