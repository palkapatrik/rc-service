package com.palkapatrik.rcs.shared

object ResponseMessages {
    const val success = "Success"
    const val error = "Something went wrong"
    const val deleted = "Deleted"
}
