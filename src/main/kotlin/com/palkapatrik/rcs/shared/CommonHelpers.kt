package com.palkapatrik.rcs.shared

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import kotlin.reflect.full.declaredMemberProperties

object CommonHelpers {
    fun buildResponse(status: HttpStatus): ResponseEntity<Unit> {
        return ResponseEntity.status(status).build()
    }

    fun Any.isAllNullInside(): Boolean {
        if (this::class.declaredMemberProperties
                .any { !it.returnType.isMarkedNullable }
        ) return false
        return this::class.declaredMemberProperties
            .none { it.getter.call(this) != null && it.getter.call(this) != "" }
    }
}
