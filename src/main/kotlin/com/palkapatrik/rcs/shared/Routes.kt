package com.palkapatrik.rcs.shared

object Routes {
    const val USERS = "/api/users"
    const val RELEASES = "/api/releases"
    const val REPOSITORIES = "/api/repositories"
    const val TASKS = "/api/tasks"
    const val HISTORY = "/api/history"
}
