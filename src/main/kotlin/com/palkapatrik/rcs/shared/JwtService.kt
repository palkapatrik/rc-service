package com.palkapatrik.rcs.shared

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.stereotype.Service
import java.lang.Exception
import java.util.*
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@Service
class JwtService {
    private val maxAge = 60 * 24 * 1000

    fun createToken(userId: String): Cookie {
        val token = Jwts.builder()
            .setIssuer(userId)
            .setExpiration(Date(System.currentTimeMillis() + maxAge))
            .signWith(SignatureAlgorithm.HS512, privateKey)
            .compact()

        val cookie = Cookie("authToken", token)
        cookie.isHttpOnly = true
        cookie.maxAge = maxAge

        return cookie
    }

    fun getIdFromToken(token: String?): String? {
        try {
            if (token === null) {
                return null
            }

            val tokenContent = Jwts.parser()
                .setSigningKey(privateKey)
                .parseClaimsJws(token)
                .body

            return tokenContent.issuer
        } catch (e: Exception) {
            return null
        }
    }

    fun removeToken(response: HttpServletResponse): HttpServletResponse {
        val cookie = Cookie("authToken", "")
        cookie.maxAge = 0
        response.addCookie(cookie)

        return response
    }
}

private const val privateKey = """MIIEowIBAAKCAQEApescBfKwaYupTdDXMgXJdFz/ubdXA/8DdLNeCU0k4dsHCumE
pP3aY95NlDdmhIaAvwYs0P5kfODRNwytPwoWzTBJ6FR4fcz38rzxwbuBghQozUhq
xc2GbRwFplDCDKDUu/pmbsv8eb6RvgqdAl+HKd5GBq9xll1wYdG1/vsjnAYiNZs9
bzwzCYL1kAySU+eNfWU39N4ym638p6Z0lO3VlbfuHjgPYdsTgtXIL2NlXPkgTB/h
0hQuyte2aHJ+du04mw8RsPYtB0wJ43FqIaMZZYwViHYfJC/khO/Ai51cIqYqJ1hi
z/EzAj0VHMlRVKKhlyL0MV9yCuQLZnVSd6oApQIDAQABAoIBAA08UCP5VDFXPs5C
Vqha5dMuiAfcpEhhU3k/xcSgLs0Fl+466qXuyjklRrMIKFlyEVsrtu+FG6cKmCX4
EOnyeBcuPvcVpfcDHtRyVibKOm6SBR87I6lb19lkXt8Bmjk/qzjDpOA5zY44sY+j
cRr7W+DvJk7qN/lywsBK5mXMyu5rPfWojqcQpyqYMRKdTLUatsi8igl3+UK2uoC1
oKGGbtalSaRho7GVVAtShYpQDMA8ag4GkJBDSpyBdPq7iTDuky6Am1SX3xNOKVSd
Lkyw6bIHP7S7AoIHhSDrPNWw/99oGpLXNIIIiLRnFAqKHEtObhZujSKk8JsS0iwr
ssTf1BECgYEAzqdxPG0XCBHUTy1sMDbKHdMmh7R9ORKPrwQk2HnyE+NIJ37/kNBE
1J8i0l1OwVPg9v9xf0rNCyTHEyBCKVpRgphVV4VuXSRepgvygwFIN2ON1EJXAeyd
+CYKMJAJ+TYPi9d/fXhl8Q6zH0d3J2BS1R08/Iyz2gAgmxmjYaM9BH8CgYEAzYmJ
LkXA+EZBw26nd7zGrAT3D4qDtKd6f2qytQWt+qRT8s51zCuvbthorxI/YswfS9lI
zFrSpxYZLu1H1yU857VeKrxQ/DFo86tNZr2tB9y7YQFvQvgCZJzF8RaECDebawvL
N4vKMXPX5OjNbBamsjAGTozeXE0w3jtL0qNU2NsCgYBiqJ3u80XjnF93zX+xJPcp
df0XHJwL+0iNjbTphI6F4CQ6du8b+9Jgj/jX5aQLuDfWs+1DkDqD73L7NpNfmrb/
WiqkNheScCKfs2R8FsibaPP1Oha/3G8uYTrMJHgeeijB4ogDJs/EXfg2HrBMJDhn
diX/jEzw7s6iPYM6wewHkQKBgQCLMBF7CXe1afaM/zlfh7JOuYoJtzabtzLoaHwe
xHreGjxTWGdDXW4PNXQm4seUA0FKXxnCcdrnlbp2Ie+djbUiAJuFe0OolVlKcf4X
jmlHwr4N4ifThRoxwkz1h9bfrbCNHE6BDiRduuJAPTsncGtwjb8noJPQk+F9DVC6
kysZMwKBgEuaOj8RJaXupnT845TdApQqkIAE5FrKhhDaV2SZnFKy8QgsOVEURJqv
pVq/MzqLAvtLch4rWq5MvxGjL0w/yMxKAvLBTVwel1Huw4WzgPk12YEAOEJP1TB7
ueMkR4gmaYG054UyERaDgfuYmoSBey36ZNMjGGOFpfNxDWMjs4za"""
