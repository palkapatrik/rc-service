package com.palkapatrik.rcs.dtos

import com.palkapatrik.rcs.task.TaskState

class TaskDTO {
    val id: String = ""
    val state: TaskState? = null
    val assignee: String = ""
    val releaseId: Int = -1
}
