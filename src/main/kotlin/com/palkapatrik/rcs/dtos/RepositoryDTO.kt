package com.palkapatrik.rcs.dtos

class RepositoryDTO {
    val name = ""
    val description = ""
    val owner = ""
}
