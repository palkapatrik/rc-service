package com.palkapatrik.rcs.dtos

import java.util.*

class ReleaseDTO {
    val name: String? = null
    val owner: String = ""
    val plannedDeployment: Date? = null
    val services: Array<String?> = emptyArray()
    val databaseSchemas: Array<String?> = emptyArray()
    val databaseComment = ""
    val additionalChanges = ""
}
