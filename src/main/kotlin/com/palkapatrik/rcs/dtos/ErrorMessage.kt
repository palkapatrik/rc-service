package com.palkapatrik.rcs.dtos

class ErrorMessage(val code: Int,  val codeName: String, val message: String) {
}
