package com.palkapatrik.rcs.repository

import java.util.*
import javax.persistence.*

@Entity
class Repository {

    @Id
    @GeneratedValue
    var id: UUID = UUID.randomUUID()

    @Column(unique = true)
    var name = ""

    @Column
    var description = ""

    @Column
    var owner = ""

    @Column
    var lastRelease = ""
}
