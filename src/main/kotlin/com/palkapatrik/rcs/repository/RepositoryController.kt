package com.palkapatrik.rcs.repository

import com.palkapatrik.rcs.dtos.ErrorMessage
import com.palkapatrik.rcs.dtos.RepositoryDTO
import com.palkapatrik.rcs.shared.CommonHelpers
import com.palkapatrik.rcs.shared.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Routes.REPOSITORIES)
class RepositoryController {
    @Autowired
    lateinit var repoService: RepositoryService

    @GetMapping("/")
    fun getAllRepos() = repoService.getAllRepos()

    @GetMapping("/{id}")
    fun getRepoById(@PathVariable id: String) = repoService.getRepoById(id)

    @GetMapping("/owner/{ownerId}")
    fun getAllReposByOwner(@PathVariable ownerId: String) = repoService.getAllByOwner(ownerId)

    @PostMapping("/add")
    fun addRepo(@RequestBody repoData: RepositoryDTO): ResponseEntity<Any> {
        val repositoryData = repoService.createNewRepo(repoData)
        return if (repositoryData != null) {
            ResponseEntity.ok().body(repoData)
        } else {
            ResponseEntity.badRequest()
                .body(
                    ErrorMessage(400, "NAME_NOT_UNIQUE", "Repository name is already in use")
                )
        }
    }

    @PutMapping("/update/{id}")
    fun updateRepo(@PathVariable id: String, @RequestBody repoData: RepositoryDTO): ResponseEntity<Unit> {
        return CommonHelpers.buildResponse(HttpStatus.NOT_IMPLEMENTED)
    }

    @DeleteMapping("/delete/{id}")
    fun deleteRepo(@PathVariable id: String): ResponseEntity<Unit> {
        return CommonHelpers.buildResponse(HttpStatus.NOT_IMPLEMENTED)
    }
}
