package com.palkapatrik.rcs.repository

import com.palkapatrik.rcs.dtos.RepositoryDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class RepositoryService {
    @Autowired
    lateinit var repoRepository: RepositoryRepo

    fun getAllRepos(): Iterable<Repository> = repoRepository.findAll()

    fun getRepoById(id: String): Optional<Repository> = repoRepository.findById(id)

    fun getAllByOwner(ownerId: String): Iterable<Repository> = repoRepository.findAllByOwner(ownerId)

    fun createNewRepo(data: RepositoryDTO): Repository? {
        val nameUsed = repoRepository.existsRepositoryByName(data.name)

        return if (!nameUsed) {
            val repository = Repository()
            repository.name = data.name
            repository.description = data.description
            repository.owner = data.owner

            repoRepository.save(repository)
            repository
        } else {
            null
        }
    }
}
