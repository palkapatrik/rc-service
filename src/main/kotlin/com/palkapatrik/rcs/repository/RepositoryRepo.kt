package com.palkapatrik.rcs.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface RepositoryRepo: JpaRepository<Repository, String> {
    @Query("SELECT r FROM Repository r WHERE r.owner = :ownerId")
    fun findAllByOwner(@Param("ownerId") ownerId: String): Iterable<Repository>

    fun existsRepositoryByName(@Param("name") name: String): Boolean
}
