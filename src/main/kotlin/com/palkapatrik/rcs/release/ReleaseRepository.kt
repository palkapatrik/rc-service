package com.palkapatrik.rcs.release

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*
import javax.transaction.Transactional

interface ReleaseRepository : JpaRepository<Release, Int> {
    @Query("SELECT r FROM Release r WHERE r.owner = :ownerId")
    fun getAllByCreator(@Param("ownerId") ownerId: String): Iterable<Release>

    @Modifying
    @Transactional
    @Query("UPDATE Release r SET r.name = :name, r.plannedDeployment = :plannedDeployment WHERE r.id = :id AND r.owner = :ownerId")
    fun updateReleaseById(
        @Param("name") name: String?,
        @Param("plannedDeployment") plannedDeployment: Date?,
        @Param("id") id: Int,
        @Param("ownerId") ownerId: String
    )

    @Modifying
    @Transactional
    @Query("UPDATE Release r SET r.repositories = :repositories WHERE r.id = :id")
    fun updateRepositoriesById(@Param("repositories") repos: String, @Param("id") id: Int): Int
}
