package com.palkapatrik.rcs.release

import com.palkapatrik.rcs.dtos.ReleaseDTO
import com.palkapatrik.rcs.dtos.ReleaseRepositoryDTO
import com.palkapatrik.rcs.task.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class ReleaseService {
    @Autowired
    lateinit var releaseRepository: ReleaseRepository

    @Autowired
    lateinit var taskService: TaskService

    fun getAll(): Iterable<Release> {
        return releaseRepository.findAll()
    }

    fun getById(id: Int): Release? {
        return releaseRepository.findByIdOrNull(id)
    }

    fun getAllByOwnerId(ownerId: String): Iterable<Release> {
        return releaseRepository.getAllByCreator(ownerId)
    }

    fun createRelease(data: ReleaseDTO): Release {
        val newRelease = Release()
        newRelease.name = data.name
        newRelease.owner = data.owner
        newRelease.plannedDeployment = data.plannedDeployment

        val created = releaseRepository.save(newRelease)
        taskService.createTask(created.owner, created.id)

        return created
    }

    fun updateReleaseData(id: Int, data: ReleaseDTO): Unit? {
        val oldReleaseData = releaseRepository.findByIdOrNull(id)
        return if (oldReleaseData != null) {
            val ownerId = oldReleaseData.owner

            val name = data.name ?: oldReleaseData.name
            val plannedDeployment = data.plannedDeployment ?: oldReleaseData.plannedDeployment

            return releaseRepository.updateReleaseById(name, plannedDeployment, id, ownerId)
        } else {
            null
        }
    }

    fun updateReleaseRepos(id: Int, data: Iterable<ReleaseRepositoryDTO>): String? {
        val repos: String = data.joinToString(separator = "|"){it.id}
        return if (releaseRepository.updateRepositoriesById(repos, id) > 0) {
            repos
        } else {
            null
        }
    }

    fun deleteRelease(id: Int): Unit? {
        return releaseRepository.deleteById(id)
    }
}
