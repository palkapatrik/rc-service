package com.palkapatrik.rcs.release

import com.palkapatrik.rcs.dtos.ErrorMessage
import com.palkapatrik.rcs.dtos.Message
import com.palkapatrik.rcs.dtos.ReleaseDTO
import com.palkapatrik.rcs.dtos.ReleaseRepositoryDTO
import com.palkapatrik.rcs.shared.CommonHelpers
import com.palkapatrik.rcs.shared.Routes
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(Routes.RELEASES)
class ReleaseController {
    @Autowired
    lateinit var releaseService: ReleaseService

    @GetMapping("/")
    fun getAllReleases(): Iterable<Release> = releaseService.getAll()

    @GetMapping("/{id}")
    fun getReleaseById(@PathVariable id: Int) = releaseService.getById(id)

    @GetMapping("/owner/{ownerId}")
    fun getAllByOwner(@PathVariable ownerId: String): Iterable<Release>? = releaseService.getAllByOwnerId(ownerId)

    @PostMapping("/create")
    fun createRelease(@RequestBody releaseData: ReleaseDTO): ResponseEntity<Release> {
        val newRelease = releaseService.createRelease(releaseData)
        return ResponseEntity.ok().body(newRelease)
    }

    @PutMapping("/{id}/update")
    fun updateRelease(@PathVariable id: Int, @RequestBody releaseData: ReleaseDTO): ResponseEntity<Any> {
        val updatedRelease = releaseService.updateReleaseData(id, releaseData)
        return if (updatedRelease !== null) {
            ResponseEntity.ok().body(Message("Updated!"))
        } else {
            ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(
                    ErrorMessage(404, "NOT_FOUND", "Release was not found in database")
                )
        }
    }

    @PutMapping("/{id}/repositories")
    fun updateRepositories(
        @PathVariable id: Int,
        @RequestBody repositories: Iterable<ReleaseRepositoryDTO>
    ): ResponseEntity<Any> {
        val repos = releaseService.updateReleaseRepos(id, repositories)
        return if (repos != null) {
            ResponseEntity.ok().body(repos)
        } else {
            ResponseEntity.badRequest()
                .body(
                    ErrorMessage(400, "RELEASE_UPDATE_FAILED", "Unable to update release repositories")
                )
        }
    }

    @DeleteMapping("/{id}/delete")
    fun deleteRelease(@PathVariable id: Int): ResponseEntity<Any> {
        return if (releaseService.deleteRelease(id) != null) {
            ResponseEntity.ok().body(Message("Deleted RLS-$id"))
        } else {
            ResponseEntity.badRequest()
                .body(
                    ErrorMessage(400, "CANNOT_DELETE_RELEASE", "Unable to delete RLS-$id")
                )
        }
    }
}
