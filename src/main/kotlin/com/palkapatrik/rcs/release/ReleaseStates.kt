package com.palkapatrik.rcs.release

enum class ReleaseStates(val value: String) {
    CONCEPT("Concept"),
    WAITING("Awaiting assessment"),
    APPROVED("Approved"),
    REJECTED("Rejected"),
    DEPLOYED("Deployed")
}
