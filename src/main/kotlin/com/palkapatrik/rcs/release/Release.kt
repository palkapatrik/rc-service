package com.palkapatrik.rcs.release

import java.util.*
import javax.persistence.*

@Entity
class Release{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0

    @Column(nullable = true)
    var name: String? = ""

    @Column
    var owner: String = ""

    @Column
    var state = ReleaseStates.CONCEPT

    @Column
    var created = Date(System.currentTimeMillis())

    @Column(nullable = true)
    var plannedDeployment: Date? = null

    @Column
    var deployed = ""

    @Column
    var repositories = ""

    @Column
    var databaseSchemas = ""

    @Column
    var databaseComment = ""

    @Column
    var additionalChanges = ""
}
